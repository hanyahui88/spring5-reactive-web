package com.example;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by alvin on 2017/4/21.
 */
@Data
public class Person implements Serializable {
  private String name;
  private int age;

  public Person(String name) {
    this.name = name;
  }
}
