package com.example;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.adapter.HttpWebHandlerAdapter;

import reactor.core.publisher.Mono;
import reactor.ipc.netty.http.server.HttpServer;

@SpringBootApplication
@RestController
public class DemoApplication {
  public static void main(String[] args) throws InterruptedException {
    HandlerFunction handlerFunction = request -> ServerResponse.ok().body(Mono.just("hello"), String.class);
    RouterFunction routerFunction = RouterFunctions.route(RequestPredicates.GET("/"), handlerFunction)
        .andRoute(RequestPredicates.GET("/json"),
            request -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Person("hahah")), Person.class));
    HttpWebHandlerAdapter httpWebHandlerAdapter = RouterFunctions.toHttpHandler(routerFunction);
    HttpServer.create("localhost", 8080)
        .newHandler(new ReactorHttpHandlerAdapter(httpWebHandlerAdapter)).block();
    //保证一直运行
    Thread.currentThread().join();
  }

}
